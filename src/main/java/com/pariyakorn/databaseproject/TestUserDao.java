/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pariyakorn.databaseproject;

import com.pariyakorn.databaseproject.dao.UserDao;
import com.pariyakorn.databaseproject.helper.DatabaseHelper;
import com.pariyakorn.databaseproject.model.User;

/**
 *
 * @author acer
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }

        for (User u : userDao.getAll(" user_name like 'u%' ", " user_name asc, user_gender desc")) {
            System.out.println(u);
        }
        DatabaseHelper.close();
    }
}
